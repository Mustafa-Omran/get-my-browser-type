<div id="process" class="w3-text-grey w3-center w3-navbar w3-xxxlarge w3-border w3-border-green">
           
        <?php
        function get_type($type)
        {
            $chrome="<a href='https://www.google.com/chrome/browser/desktop/' class='w3-hover-text-black fa fa-chrome'> Chrome</a>";
            $edge="<a href='https://www.microsoft.com/en-us/download/details.aspx?id=48126'class='w3-hover-text-black fa fa-edge'> edge</a>";
            $opera="<a href='http://www.opera.com/ar' class='w3-hover-text-black fa fa-opera'> opera</a>";
            $safari="<a href='https://support.apple.com/downloads/safari' class='w3-hover-text-black fa fa-safari'> safari</a>";
            $firefox="<a href='https://www.mozilla.org/en-US/firefox/new/' class='w3-hover-text-black fa fa-firefox'> mozilla firefox</a>";
            $string="Browser type : ";
            
            if (strpos($type, 'Opera') || strpos($type, 'OPR/')) 
             {
                   return $string.'<span class="w3-text-black">Opera</span>'.' & Icon :'.' <i class="fa fa-opera w3-text-black"></i>'.'<br>'.$chrome.'<br>'.$edge.'<br>'.$safari.'<br>'.$firefox;  
             }
             
            elseif (strpos($type, 'Edge'))
            {
                    return $string.'<span class="w3-text-black">Edge</span>'.' & Icon :'.' <i class="fa fa-edge w3-text-black"></i>'.'<br>'.$chrome.'<br>'.$opera.'<br>'.$safari.'<br>'.$firefox;
            }
            elseif (strpos($type, 'Chrome'))
            {
                    return $string.'<span class="w3-text-black">Chrome</span>'.' & Icon :'.' <i class="fa fa-chrome w3-text-black"></i>'.'<br>'.$edge.'<br>'.$opera.'<br>'.$safari.'<br>'.$firefox;
            }
            elseif (strpos($type, 'Safari')) 
            {
                    return $string.'<span class="w3-text-black">Safari</span>'.' & Icon :'.' <i class="fa fa-safari w3-text-black"></i>'.'<br>'.$chrome.'<br>'.$opera.'<br>'.$edge.'<br>'.$firefox;
            }
            elseif (strpos($type, 'Firefox')) 
            {
                    return $string.'<span class="w3-text-black">Firefox</span>'.' & Icon :'.' <i class="fa fa-firefox w3-text-black"></i>'.'<br>'.$chrome.'<br>'.$opera.'<br>'.$safari.'<br>'.$edge;
                    
            }
            elseif (strpos($type, 'MSIE') || strpos($type, 'Trident/7'))
            {
                    return $string.'<span class="w3-text-black">Internet Explorer</span>'.' & Icon :'.' <i class="fa fa-internet-explorer" w3-text-black></i>'.'<br>'.'Oops Internet explorer'.'<br>'.$chrome.'<br>'.$edge.'<br>'.$firefox.'<br>'.$opera.'<br>'.$safari;
            }

            return 'Other';
        }


        echo get_type($_SERVER['HTTP_USER_AGENT']);
        ?>
            
        </div>