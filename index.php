<!DOCTYPE html>

<!--
                       Mustafa Omran
                  --get browser type ! --
       -- if your and let you download other browsers--
-->

<html>
    
    <head>
        <meta charset="UTF-8">
        <title>Find my Browser type !</title>
        <!--  w3.css style sheet library-->
        <link href="lib/w3.css" rel="stylesheet">
        
        <link href="css/style.css" rel="stylesheet">
        
        <!--- font awesome library-->
        <link href="css/font-awesome.min.css" rel="stylesheet">
    </head>
    
    <body class="w3-container w3-light-grey w3-row">
        
       <?php
       // Header 
       include './includes/header.php';
       ?>
        
       <?php 
       // Process
       include './includes/process.php';
       ?>

        <?php
        // Footer
        include './includes/footer.php';
        ?>
    </body>
</html>
